import { test, expect } from '@playwright/test';
const path = require('node:path'); 

test('has title', async ({ page }) => {
  await page.goto('https://canap.epfl.ch/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Postulation Apprentis | Accueil/);
  // Take a screenshot
  await page.screenshot({ path: 'screenshot.png', fullPage: true });
  // IDEA: compare screenshot with https://github.com/mapbox/pixelmatch
});

test('home page buttons', async ({ page }) => {
  await page.goto('https://canap.epfl.ch/');

  await expect(page.getByRole('button', { name: 'Créer un compte' })).toBeVisible();
  await expect(page.getByRole('button', { name: 'Se connecter' })).toBeVisible();
  await expect(page.getByRole('button', { name: 'Consulter ma postulation' })).toBeVisible();

});

test('add application', async ({ page }) => {

  // Go to canap home page and click "se connecter"
  await page.goto('https://canap.epfl.ch/');
  await page.getByRole('button', { name: 'Se connecter' }).click();

  // Login as an applicant
  await page.locator('#username').fill(process.env.GUEST_ACCOUNT_MAIL);
  await page.locator('#password').fill(process.env.GUEST_ACCOUNT_PASS);
  await page.locator('#loginbutton').click();

  // On the form
  await page.locator('select[name="job"]').selectOption('67');
  await page.getByText('Candidature pour un apprentissage 1. Apprentissage Je suis intéressé par la').click();
  await page.locator('#all div').filter({ hasText: 'Je désire m\'inscire en' }).locator('div').first().click();
  await page.getByText('Oui').first().click();
  // await page.locator('#genreApp').selectOption('Femme');
  await page.locator('#genreApp').selectOption('Homme');
  await page.locator('#adrApp').fill('Ch. des machines');
  await page.locator('#NPAApp').fill('1000, Lausanne');

  await page.locator('#telApp').fill('+41 21 693 1234');
  await page.locator('#phoneApp').fill('+41 21 693 1234');
  await page.locator('#birthApp').fill('01/12/2010');

  await page.locator('#originApp').fill('Lausanne');
  await page.locator('#nationApp').fill('Suisse');
  await page.locator('#langApp').fill('Français');
  await page.locator('#avsNumber').fill('0000 0000 0000 0000');
  await page.getByText('Français').click();
  await page.getByText('Allemand').click();
  await page.getByText('Anglais').click();
  await page.getByText('Autres').click();

  await page.getByText('Oui').nth(1).click();

  await page.locator('input[name="ecole1"]').fill('École');
  await page.locator('input[name="lieuEcole1"]').fill('Lausanne');
  await page.locator('input[name="anneesEcole1"]').fill('2020-2022');
  await page.locator('input[name="ecole2"]').fill('École');
  await page.locator('input[name="lieuEcole2"]').fill('Lausanne');
  await page.locator('input[name="anneesEcole2"]').fill('2022-2024');
  await page.locator('#anneeFin').fill('2024');

  await page.locator('input[name="activiteStage1"]').fill('Informaticien·ne');
  await page.locator('input[name="entrepriseStage1"]').fill('EPFL');
  await page.locator('input[name="activiteStage2"]').fill('Informaticien·ne');
  await page.locator('input[name="entrepriseStage2"]').fill('Qoqa');

  await page.getByText('Non').nth(2).click();

  await page.getByLabel('Photo passeport couleur: *').setInputFiles(path.join(__dirname, 'cat.jpg'));
  await page.getByLabel('Copie carte d\'indentité /').setInputFiles(path.join(__dirname, 'myfile.pdf'));

  await page.getByLabel('Curriculum Vitae: *').setInputFiles(path.join(__dirname, 'myfile.pdf'));
  await page.getByLabel('Lettre de motivation: *').setInputFiles(path.join(__dirname, 'myfile.pdf'));

  await page.getByText('Accepter les conditions*').click();
  await page.waitForTimeout(2000);
  await page.getByRole('button', { name: 'Terminer' }).click();

  // PAGE EXPECT
  // Votre demande à bien été enregistrée, vous allez bientôt recevoir un e-mail confirmant votre postulation.
  // Vous avez la possibilité de consulter votre postulation depuis la page d'accueil de ce formulaire
  await page.waitForTimeout(2000);
  await page.getByRole('button', { name: 'Terminer' }).click();

});

test('remove application', async ({ page }) => {

  // Go to canap home page and click "se connecter"
  await page.goto('https://canap.epfl.ch/');
  await page.getByRole('button', { name: 'Consulter ma postulation' }).click();
  await page.waitForTimeout(2000);
  // Navigate to Tequila
  await page.locator('#username').fill(process.env.GUEST_ACCOUNT_MAIL); 
  await page.locator('#password').fill(process.env.GUEST_ACCOUNT_PASS);
  await page.locator('#loginbutton').click();
  // Should be back to canap.epfl.ch
  await page.waitForTimeout(1000);

  const noPostulationAvailable = await page.getByText('Aucune postulation effectuée.').isVisible() ;

  if (noPostulationAvailable) {
    console.log("Aucune postulation effectuée.");
    await expect(page.getByText('Aucune postulation effectuée')).toBeVisible();
  } else {
    // This would be ideal but I can't make it work
    // await page.getByRole('button', { name: 'Retirer ma postulation' }).click();
    // page.on('dialog', async dialog => {
    //   //expect(dialog.type()).toContain('confirm')
    //   //expect(dialog.message()).toContain('Voulez-vous vraiment supprimer cette postulation')
    //   //await page.getByRole('button', { name: 'Retirer ma postulation' }).click();
    //   await dialog.accept();
    // });
    // ... so plan B is to call viewpostulation.php?delete
    await page.goto('https://canap.epfl.ch/viewpostulation.php?delete');
    await expect(page.getByText('Aucune postulation effectuée')).toBeVisible();
  }
  //await page.close({ runBeforeUnload: true });

});
