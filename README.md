# CanAp Tests

Code for end-to-end testing canap.epfl.ch with https://playwright.dev/.


## Usage

### First installation
```bash
git clone git@gitlab.epfl.ch:si-idevfsd/canap.test && cd canap.test
pnpm i
```

### Configuration

Edit the `.env` file based on `.env.sample` :
```bash
cp .env.sample .env
$EDITOR .env
```

### Run tests

```bash
pnpm exec playwright test
pnpm exec playwright show-report
```


## Info

Inside that directory, you can run several commands:

  `pnpm exec playwright test`  
    Runs the end-to-end tests.

  `pnpm exec playwright test --ui`  
    Starts the interactive UI mode.

  `pnpm exec playwright test --project=chromium`  
    Runs the tests only on Desktop Chrome.

  `pnpm exec playwright test canap`  
    Runs the tests in a specific file.

  `pnpm exec playwright test --debug`  
    Runs the tests in debug mode.

  `pnpm exec playwright codegen`  
    Auto generate tests with Codegen.

We suggest that you begin by typing:

    `pnpm exec playwright test`

And check out the following files:
  - `./example.spec.ts` - Example end-to-end test
  - `./tests-examples/demo-todo-app.spec.ts` - Demo Todo App end-to-end tests
  - `./playwright.config.ts` - Playwright Test configuration
